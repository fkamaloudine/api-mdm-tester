package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.helper.MdmMapping;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.AddressMDM;
import com.accor.rcu.model.profile.ContactMDM;
import com.accor.rcu.model.profile.EmailMDM;
import com.accor.rcu.model.profile.Location;
import com.accor.rcu.model.profile.PhoneMDM;
import com.accor.rcu.model.relationship.ContactSNMDM;
import com.accor.rcu.model.saleagreement.ProfessionalContract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class CreateContactService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateContactService.class);

    static boolean executeCreateContact(ContactMDM contactData) {
        LOGGER.info("CreateContactMDM");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
        int applicationId = 35;
        boolean result = false;
        try {
            ContactMDM profile = rcuServiceCaller.createContactMDM(applicationId, contactData);
            MdmMapping.afficherContactMDM(profile, LOGGER);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        }
        LOGGER.info("CreateContactMDM-End");
        return result;
    }

    static ContactMDM build(String socialNetworkUid) {
        Date ouptOutDate = null;
        String loginEmail = "mojo1.test@me.com";
        String civility = "MR";
        String firstName = "Un nom quelconque";
        String lastName = "Un nom quelconque";
        String language = "fr";
        AddressMDM addressData = new AddressMDM();
        PhoneMDM phoneData = new PhoneMDM();
        ProfessionalContract professionalContract = new ProfessionalContract();
        Date birthday = null;
        String password = "passwd11";
        String registrationChannel = "WEB";

        phoneData.setPhoneNumber("0624648736");
        phoneData.setPhonePrefix("33");

        phoneData.setPhoneNPAI(0);

        professionalContract.setCompanyID("");

        final Location location = new Location(addressData, getPhoneMDMOne());

        final ContactMDM contactData = new ContactMDM();
        contactData.setLocation(location);
        contactData.setOptOut(0);
        contactData.setOptOutDate(ouptOutDate);
        contactData.setFirstName(firstName);
        contactData.setLastName(lastName);
        contactData.setLanguage(language);
        contactData.setCivility(civility);
        contactData.setPassword(password);
        contactData.setBirthday(birthday);
        contactData.setRegistrationSite("");
        contactData.setRegistrationChannel(registrationChannel);
        contactData.setLoginEmail(loginEmail);
        List<AddressMDM> lAddressMDMs = new ArrayList<>();
        lAddressMDMs.add(getAddressOne());
        contactData.setlAddressMDMs(lAddressMDMs);
        List<PhoneMDM> lPhoneMDMs = new ArrayList<>();
        lPhoneMDMs.add(getPhoneMDMOne());
        contactData.setlPhoneMDMs(lPhoneMDMs);
        List<EmailMDM> lEmailMDMs = new ArrayList<>();
        List<ContactSNMDM> lContactSNMDMs = new ArrayList<>();
        lContactSNMDMs.add(getContactSNMDMOne(birthday, socialNetworkUid));
        contactData.setlEmailMDMs(lEmailMDMs);
        contactData.setlContactSNMDMs(lContactSNMDMs);
        //MdmMapping.afficherContactMDM(contactData, LOGGER);
        return contactData;
    }


    private static PhoneMDM getPhoneMDMOne() {
        final PhoneMDM phoneData = new PhoneMDM();

        phoneData.setPhonePrefix("33");
        // phoneData.setPhoneNPAI(1);
        // phoneData.setForcageFlag(0);
        // phoneData.setPhoneNPAIDate(this.birthday);
        phoneData.setPhoneNumber("0626761692");
        // phoneData.setPhoneOptOut(1);
        // phoneData.setPhoneOptOutDate(this.birthday);

        phoneData.setPhoneType("Personal");
        // phoneData.setPrimary(1);

        phoneData.setUsageCode("Cell");
        return phoneData;
    }


    private static AddressMDM getAddressOne() {
        final AddressMDM addressData = new AddressMDM();

        addressData.setCompanyName("");

        addressData.setExtensionAddress("");
        addressData.setAddress1("une address quelconque");
        addressData.setAddress2("");

        addressData.setAddressNPAI(0);

        addressData.setCity("Paris");
        addressData.setCountry("FR");
        addressData.setZipCode("");
        addressData.setType("1");

        return addressData;
    }


    private static ContactSNMDM getContactSNMDMOne(Date birthday, String uid) {
        final ContactSNMDM snmdm = new ContactSNMDM();

        snmdm.setBrand("NOV");
        snmdm.setSnNPAIDate(birthday);
        snmdm.setSnNPAIFlag(1);
        snmdm.setSnOptOut(1);
        snmdm.setSnOptOutDate(birthday);
        snmdm.setSocialNetworkType("FACEBOOK");
        snmdm.setToken("tokenw");
        snmdm.setTokenDate(birthday);
        snmdm.setUID(uid);
        return snmdm;
    }


}
