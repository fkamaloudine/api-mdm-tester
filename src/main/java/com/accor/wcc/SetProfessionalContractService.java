package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.saleagreement.ProfessionalContract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

class SetProfessionalContractService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SetProfessionalContractService.class);

    static boolean executeSetProfessionalContract(String contactId) {
        LOGGER.info("SetProfessionalContract");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
        int applicationId = 35;

        ProfessionalContract professionalContract = new ProfessionalContract();
        professionalContract.setCompanyID("SC910CAPC452064");
        // professionalContract.setCompanyName("CARROUF786");
        professionalContract.setContractID("247CAPCAP883");
        boolean result = false;
        try {
            int code = rcuServiceCaller.setProfessionalContract(applicationId, Long.parseLong(contactId), professionalContract);
            LOGGER.info("Returned code: {}", code);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        }
        LOGGER.info("SetProfessionalContract-End");
        return result;
    }
}
