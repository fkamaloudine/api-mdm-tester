package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.ContactMDM;
import com.accor.rcu.model.profile.PhoneMDM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

class SetContactService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SetContactService.class);

    static boolean executeSetContact(ContactMDM contactData) {
        LOGGER.info("SetContactMDM");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
        boolean result = false;
        try {
            int applicationId = 35;
            int code = rcuServiceCaller.setContactMDM(applicationId, contactData);
            LOGGER.info("Returned code: {}", code);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        }
        LOGGER.info("SetContactMDM-End");
        return result;
    }

    static ContactMDM build(String contactId) {
        ContactMDM contactData = new ContactMDM();
        String firstName = "Yannick";
        contactData.setFirstName(firstName);
        contactData.setFirstNameUpper(firstName.toUpperCase());
        String lastName = "SAGSETe";
        contactData.setLastName(lastName);
        contactData.setLastNameUpper(lastName.toUpperCase());
        contactData.setContactID(Long.parseLong(contactId));


        PhoneMDM phoneData = new PhoneMDM();
        phoneData.setPhonePrefix("33");
        phoneData.setPhoneNumber("0626761692");
        phoneData.setUsageCode("Cell");
        phoneData.setPhoneType("Professional");
        List<PhoneMDM> lPhoneMDMs = new ArrayList<>();
        lPhoneMDMs.add(phoneData);
        //contactData.setlPhoneMDMs(lPhoneMDMs); // uncomment if you want to add phone

        return contactData;
    }

}
