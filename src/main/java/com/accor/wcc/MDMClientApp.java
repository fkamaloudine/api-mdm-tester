package com.accor.wcc;


import static com.accor.wcc.ActivateRequestedEmailService.executeActivateRequestedEmail;
import static com.accor.wcc.CreateContactService.executeCreateContact;
import static com.accor.wcc.DeleteProfessionalContractService.executeDeleteProfessionalContract;
import static com.accor.wcc.GetContactService.executeGetContact;
import static com.accor.wcc.SetContactOptoutService.executeSetContactOptout;
import static com.accor.wcc.SetContactService.executeSetContact;
import static com.accor.wcc.SetProfessionalContractService.executeSetProfessionalContract;
import static com.accor.wcc.UpsertIdentityPaperService.executeUpsertIdentityPaper;
import static com.accor.wcc.UpsertWalletService.executeUpsertWallet;

public class MDMClientApp {

    public static boolean launch(String[] args) {
        return myMethod(args);
    }

    public static void main(String[] args) {
        myMethod(args);
    }

    private static boolean myMethod(String[] args) {
        if (args.length == 0) {
            showUsage();
            System.exit(0);
        }

        boolean result = false;

        switch (args[0].toUpperCase()) {
            case "CREATECONTACT":
                if (checkRequiredArgs(args, 2)) {
                    result = executeCreateContact(CreateContactService.build(args[1]));
                }
                break;
            case "SETCONTACT":
                if (checkRequiredArgs(args, 2)) {
                    result = executeSetContact(SetContactService.build(args[1]));
                }
                break;
            case "GETCONTACT":
                if (checkRequiredArgs(args, 2)) {
                    result = executeGetContact(Long.parseLong(args[1]));
                }
                break;
            case "ACTIVATEREQUESTEDEMAIL":
                if (checkRequiredArgs(args, 1)) {
                    result = executeActivateRequestedEmail();
                }
                break;
            case "SETCONTACTOPTOUT":
                if (checkRequiredArgs(args, 2)) {
                    result = executeSetContactOptout(args[1]);
                }
                break;
            case "SETPROFESSIONALCONTRACT":
                if (checkRequiredArgs(args, 2)) {
                    result = executeSetProfessionalContract(args[1]);
                }
                break;
            case "UPSERTWALLET":
                if (checkRequiredArgs(args, 2)) {
                    result = executeUpsertWallet(args[1]);
                }
                break;
            case "DELETEPROFESSIONALCONTRACT":
                if (checkRequiredArgs(args, 2)) {
                    result = executeDeleteProfessionalContract(args[1]);
                }
                break;
            case "UPSERTIDENTITYPAPER":
                if (checkRequiredArgs(args, 2)) {
                    result = executeUpsertIdentityPaper(args[1]);
                }
                break;
            default:
                showUsage();
                System.exit(0);
        }
        return result;
    }


    private static void showUsage() {
        System.out.printf(USAGE);
    }

    private static final String USAGE = "Usage : java -jar <jar-file> followed by one of : %n" +
            "CreateContact <socialNetworkId> Ex: CreateContact kissSocialNetId1 %n" +
            "SetContact <contactId> Ex: SetContact 100001083493710 %n" +
            "GetContact <contactId> Ex: GetContact 100001083493710 %n" +
            "ActivateRequestedEmail Ex: ActivateRequestedEmail %n" +
            "SetContactOptout <contactId> Ex: SetContactOptout 100001083493710 %n" +
            "SetProfessionalContract <contactId> Ex: SetProfessionalContract 100001083493710 %n" +
            "UpsertWallet <pmid> Ex: UpsertWallet 0383149A %n" +
            "DeleteProfessionalContract <contactId> Ex: DeleteProfessionalContract 100001083493710 %n" +
            "UpsertIdentityPaper <contactId> Ex: UpsertIdentityPaper 100001083493710 %n";

    private static boolean checkRequiredArgs(String[] args, int requiredLength) {
        if (args.length >= requiredLength) {
            return true;
        } else {
            showUsage();
        }
        return false;
    }

}
