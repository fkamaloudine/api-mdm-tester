package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.NationalIdentifierMDM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

class UpsertIdentityPaperService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpsertIdentityPaperService.class);

    static boolean executeUpsertIdentityPaper(String contactId) {
        LOGGER.info("UpsertIdentityPaper");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
        int applicationId = 35;
        String pmid = "";

        boolean result = false;
        try {
            int code = rcuServiceCaller.upsertIdentityPaper(applicationId, pmid, contactId, getLstIdentityPaper());
            LOGGER.info("Returned code: {}", code);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        } catch (ParseException e) {
            LOGGER.info("Cannot parse the date", e);
        }
        LOGGER.info("UpsertIdentityPaper-End");
        return result;
    }

    private static ArrayList<NationalIdentifierMDM> getLstIdentityPaper() throws ParseException {
        ArrayList<NationalIdentifierMDM> lstIdentityPaper = new ArrayList<>();
        NationalIdentifierMDM ni1 = new NationalIdentifierMDM();

        ni1.setValidationDate(new SimpleDateFormat("MM/dd/yyyy").parse("12/31/2015"));
        ni1.setExpirationDate(new SimpleDateFormat("MM/dd/yyyy").parse("01/01/2025"));
        ni1.setIssuedDate(new SimpleDateFormat("MM/dd/yyyy").parse("01/01/2025"));
        ni1.setIssuingCountry("FR");
        ni1.setNationalityCode("FR");
        // Ajout : id vide et nouveau numéro non existant (=nouvelle adresse)
        // Suppression : id défini et existant et adresse vide
        // Modification : id défini et existant et adresse modifiée
        ni1.setNumero("test02");
        ni1.setId("");
        ni1.setTypeCode("Active");
        ni1.setUseType("PAS");

        lstIdentityPaper.add(ni1);

        return lstIdentityPaper;
    }
}
