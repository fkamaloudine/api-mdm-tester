package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

class SetContactOptoutService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SetContactOptoutService.class);
    private static final DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

    static boolean executeSetContactOptout(String contactId) {
        LOGGER.info("SetContactOptout");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
        int applicationId = 35;

        boolean result = false;
        try {
            Date date = formatter.parse("10/02/2013");
            int code = rcuServiceCaller.setContactOptout(applicationId, Long.parseLong(contactId), date);
            LOGGER.info("Returned code: {}", code);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        } catch (ParseException e) {
            LOGGER.info("Cannot parse the date", e);
        }
        LOGGER.info("SetContactOptout-End");
        return result;
    }
}
