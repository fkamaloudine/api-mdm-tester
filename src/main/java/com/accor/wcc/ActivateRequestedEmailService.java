package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

class ActivateRequestedEmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ActivateRequestedEmailService.class);

    static boolean executeActivateRequestedEmail() {
        LOGGER.info("ActivateRequestedEmail");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
        String identifierKey = "f2949836eb58be49f9ca349be8e01d811b5ab9c7bf9141bb9694dbf1348146f4";
        String requestedEmail = "reqEmail@test.fr";
        int applicationId = 35;
        boolean result = false;
        try {
            int code = rcuServiceCaller.activateRequestedEmail(applicationId, identifierKey, requestedEmail);
            LOGGER.info("Returned code: {}", code);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        }
        LOGGER.info("ActivateRequestedEmail-End");
        return result;
    }
}
