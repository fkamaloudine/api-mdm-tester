package com.accor.wcc;

import com.accor.rcu.client.profile.RcuServiceCaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ContextLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContextLoader.class);

    private static ContextLoader ourInstance = new ContextLoader();

    public static ContextLoader getInstance() {
        return ourInstance;
    }

    private ApplicationContext applicationContext;
    private Properties props;


    private ContextLoader() {
        props = new Properties();
        ClassPathResource resource = new ClassPathResource("tester-app-values.properties");
        try (InputStream inputStream = resource.getInputStream()) {
            props.load(inputStream);
            applicationContext = new ClassPathXmlApplicationContext(getClientConfigProperty());
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    public RcuServiceCaller getContactServiceCallerApi() {
        return (RcuServiceCaller) applicationContext.getBean(getRcuServiceCallerProperty());
    }


    public String getRcuServiceCallerProperty() {
        return props.getProperty("rcu.service.caller");
    }

    public String getClientConfigProperty() {
        return props.getProperty("client.config.location");
    }
}
