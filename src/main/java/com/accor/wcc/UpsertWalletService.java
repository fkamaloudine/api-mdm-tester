package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.WalletMDM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

class UpsertWalletService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpsertWalletService.class);

    static boolean executeUpsertWallet(String pmid) {
        LOGGER.info("UpsertWalletMDM");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();

        int applicationId = 35;
        ArrayList<WalletMDM> wList = new ArrayList<>();
        boolean result = false;
        try {
            WalletMDM w1 = new WalletMDM();
            w1.setWalletId("110e8400-e29b-11d4-a716-446655440028");
            w1.setStartUse(new SimpleDateFormat("MM/dd/yyyy").parse("01/01/2016"));
            w1.setStatus("Active");
            wList.add(w1);
            int code = rcuServiceCaller.upsertWalletMDM(pmid, applicationId, wList);
            LOGGER.info("Returned code: {}", code);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        } catch (ParseException e) {
            LOGGER.info("Cannot parse the date", e);
        }
        LOGGER.info("UpsertWalletMDM-End");
        return result;
    }
}
