package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

class DeleteProfessionalContractService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteProfessionalContractService.class);

    static boolean executeDeleteProfessionalContract(String contactId) {
        LOGGER.info("DeleteProfessionalContractMDM");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
        int applicationId = 35;

        String accIntegrationId = "33362900";
        String contractNumber = "08080808";
        boolean result = false;
        try {
            int code = rcuServiceCaller.deleteProfessionalContractMDM(applicationId, Long.parseLong(contactId), accIntegrationId, contractNumber);
            LOGGER.info("Returned code: {}", code);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        }
        LOGGER.info("DeleteProfessionalContractMDM-End");
        return result;
    }
}
