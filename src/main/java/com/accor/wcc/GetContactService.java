package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.helper.MdmMapping;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.ContactMDM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

class GetContactService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetContactService.class);

    static boolean executeGetContact(Long contactId) {
        LOGGER.info("GetContactMDM");
        RcuServiceCaller rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
        boolean result = false;
        try {
            int applicationId = 35;
            ContactMDM contactMDM = rcuServiceCaller.getContactMDM(applicationId, contactId);
            MdmMapping.afficherContactMDM(contactMDM, LOGGER);
            result = true;
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        }
        LOGGER.info("GetContactMDM-End");
        return result;
    }

}
