package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SetContactOptOutTest extends AbstractRcuBaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SetContactOptOutTest.class);
    private final int application = 10;
    private final Long contactID = 31004443L;

    private final static DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");


    @Test
    public void testContactOptOut() throws Exception {
        LOGGER.info("--Debut du test testsetLocation--");
        final Date d = SetContactOptOutTest.formatter.parse("10/02/2013");
        int i = -1;

        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);
        try {

            i = contactServiceCaller.setContactOptout(this.application, this.contactID, d);
        } catch (final RcuException e) {
            final List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (final ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }
        }

        Assert.assertEquals(0, i);
        LOGGER.info("Code retour : " + i);
        LOGGER.info("--Fin du test testsetLocation--");
    }



}
