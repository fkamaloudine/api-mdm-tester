package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.saleagreement.ProfessionalContract;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SetProfessionalContractTest extends AbstractRcuBaseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetProfessionalContractTest.class);

    private ProfessionalContract professionalContract = new ProfessionalContract();

    @Test
    public void testCreateContact() throws Exception {
        LOGGER.info("--Debut du test testCreateContact--");

        this.professionalContract.setCompanyID("SC910CAPC452064");
        // professionalContract.setCompanyName("CARROUF786");
        this.professionalContract.setContractID("247CAPCAP883");

        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);
        int i = -1;
        try {

            int APPLICATION_ID = 10;
            Long profilContactID = 31004722L;
            i = contactServiceCaller.setProfessionalContract(APPLICATION_ID, profilContactID, this.professionalContract);
        } catch (final RcuException e) {
            final List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (final ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }

        } catch (final Exception e) {
            LOGGER.info("error :" + e.getMessage());

        }

        Assert.assertEquals(0, i);
        LOGGER.info("--Fin du test testCreateContact--");
    }

}
