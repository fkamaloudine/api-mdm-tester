package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.WalletMDM;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class UpsertWalletMDMTest extends AbstractRcuBaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpsertWalletMDMTest.class);

    @Test
    public void testUpsertWalletMDM() throws Exception {
        LOGGER.info("--Debut du test testUpsertWalletMDM --");

        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);
        int applicationId = 10;
        LOGGER.info("UpsertWalletMDMTest - applicationId: " + applicationId);
        // public final String pmid = "02582117";
        String pmid = "";
        LOGGER.info("UpsertWalletMDMTest - pmid: " + pmid);

        int result = -1;
        try {
            List<WalletMDM> wList = new ArrayList<>();

            WalletMDM w1 = new WalletMDM();
            String walletId = "110e8400-e29b-11d4-a716-446655440028";
            w1.setWalletId(walletId);
            w1.setStartUse(new SimpleDateFormat("MM/dd/yyyy").parse("01/01/2016"));
            String status = "Active";
            w1.setStatus(status);

            wList.add(w1);

            result = contactServiceCaller.upsertWalletMDM(pmid, applicationId, wList);

            LOGGER.info("UpsertWalletMDMTest - result: " + result);

        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            for (ErrorItem errorItem : errors) {
                LOGGER.info("error code :" + errorItem.getErrorCode());
                LOGGER.info("error Message :" + errorItem.getErrorInfo());
                LOGGER.info("error Field :" + errorItem.getErrorField());
                LOGGER.info("error Index :" + errorItem.getErrorIndex());
                Assert.assertEquals(0, errorItem.getErrorCode());
            }

        }

        Assert.assertEquals(0, result);
        LOGGER.info("--Fin du test testUpsertWalletMDM--");

    }

}
