package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.NationalIdentifierMDM;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class UpsertIdentityPaperTest extends AbstractRcuBaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpsertIdentityPaperTest.class);

    @Test
    public void testUpsertIdentityPaperTest() throws Exception {

        LOGGER.info("-- Debut testUpsertIdentityPaperTest --");

        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);

        LOGGER.info("testUpsertIdentityPaperTest - ApplicationId: " + 10);
        LOGGER.info("testUpsertIdentityPaperTest - ContactID : " + "31016428");

        int returnCode = -1;
        try {
            List<NationalIdentifierMDM> lNationalIdentifier = new ArrayList<>();

            NationalIdentifierMDM ni1 = new NationalIdentifierMDM();

            ni1.setValidationDate(new SimpleDateFormat("MM/dd/yyyy").parse("12/31/2015"));
            ni1.setExpirationDate(new SimpleDateFormat("MM/dd/yyyy").parse("01/01/2025"));
            ni1.setIssuedDate(new SimpleDateFormat("MM/dd/yyyy").parse("01/01/2025"));
            ni1.setIssuingCountry("FR");
            ni1.setNationalityCode("FR");
            // Ajout : id vide et nouveau numéro non existant (=nouvelle adresse)
            // Suppression : id défini et existant et adresse vide
            // Modification : id défini et existant et adresse modifiée
            ni1.setNumero("test02");
            ni1.setId("");
            ni1.setTypeCode("Active");
            ni1.setUseType("PAS");

            lNationalIdentifier.add(ni1);

            returnCode = contactServiceCaller.upsertIdentityPaper(10, "", "31016428", lNationalIdentifier);
            LOGGER.info("-- testUpsertIdentityPaperTest return code : " + returnCode);
        } catch (RcuException e) {
            Assert.assertNotNull(e);
            List<ErrorItem> errors = e.getErrors();
            for (ErrorItem errorItem : errors) {
                LOGGER.info("error code :" + errorItem.getErrorCode());
                LOGGER.info("error Message :" + errorItem.getErrorInfo());
                LOGGER.info("error Field :" + errorItem.getErrorField());
                LOGGER.info("error Index :" + errorItem.getErrorIndex());
            }
            LOGGER.info("-- testUpsertIdentityPaperTest Error code : " + e.getCode());
            LOGGER.info("-- testUpsertIdentityPaperTest Error message : " + e.getMessage());
        }

        Assert.assertEquals(0, returnCode);
        LOGGER.info("-- testUpsertIdentityPaperTest --");

    }
}
