package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DeleteProfessionalContractMDMTest extends AbstractRcuBaseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteProfessionalContractMDMTest.class);

    @Test
    public void testDeleteProfessionalContractTest() {
        LOGGER.info("--Debut du test testDeleteProfessionalContractTest --");
        int i = -1;

        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);
        try {
            int applicationId = 10;
            Long contactId = 31002348L;
            String contractNumber = "08080808";
            String integrationId = "33362900";
            i = contactServiceCaller.deleteProfessionalContractMDM(applicationId, contactId, integrationId, contractNumber);
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }

        } catch (Exception e) {
            LOGGER.info("error :" + e.getMessage());
        }

        Assert.assertEquals(0, i);

        LOGGER.info("Code retour : " + i);

        LOGGER.info("--Fin du test testDeleteProfessionalContractTest--");
    }

}
