package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.helper.MdmMapping;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.*;
import com.accor.rcu.model.relationship.ContactSNMDM;
import com.accor.rcu.model.saleagreement.ProfessionalContract;
import com.accor.rcu.model.saleagreement.ProfessionalContractMDM;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreateContactMDMTest extends AbstractRcuBaseTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(CreateContactMDMTest.class);

	public final int APPLICATION_ID = 35;

	Date ouptOutDate = null;
	public final Long profilContactID = null;
	public final String loginEmail = "mojo1.test@me.com";
	public final String pmid = null;
	public final int primaryEmailNPAI = 1;
	public final String requestedEmail = "";
	public final boolean hasPassword = true;
	boolean mustUpdatePassword = false;
	public final Date lastConnectionDate = new Date();

	public final String civility = "MR";
	public final String firstName = "Un nom quelconque";
	public final String lastName = "Un nom quelconque";
	public final boolean emailOptOut = false;
	public final String language = "fr";
	// public final boolean isMerged = false;
	// public final boolean aclubMember = true;

	public final AddressMDM addressData = new AddressMDM();

	PhoneMDM phoneData = new PhoneMDM();

	ProfessionalContract professionalContract = new ProfessionalContract();

	Date birthday = null;

	boolean reachable = true;

	boolean hasPreference = true;

	String password = "passwd11";
	String newPassword = null;
	String registrationSite = null;
	String registrationChannel = "WEB";

	@Test
	public void testCreateContactMDM() throws Exception {
		LOGGER.info("--Debut du test testCreateContact--");
		ContactMDM c = null;

		// this.birthday = sdf.parse("");

		this.phoneData.setPhoneNumber("0624648736");
		this.phoneData.setPhonePrefix("33");

		this.phoneData.setPhoneNPAI(0);

		this.professionalContract.setCompanyID("");
		// professionalContract.setContractID("HUAW11");
		// professionalContract.setCompanyName("HUAWEI11");

		final Location location = new Location(this.addressData,
				getPhoneMDMOne());

		final ContactMDM contactData = new ContactMDM();
		contactData.setLocation(location);
		contactData.setOptOut(0);
		contactData.setOptOutDate(this.ouptOutDate);
		contactData.setFirstName(this.firstName);
		contactData.setLastName(this.lastName);
		contactData.setLanguage(this.language);
		contactData.setCivility(this.civility);
		contactData.setPassword(this.password);
		contactData.setBirthday(this.birthday);
		contactData.setRegistrationSite("");
		contactData.setRegistrationChannel(this.registrationChannel);
		contactData.setLoginEmail(this.loginEmail);
		// contactData.setMerged(true);
		final List<AddressMDM> lAddressMDMs = new ArrayList<AddressMDM>();
		lAddressMDMs.add(getAddressOne());
		// lAddressMDMs.add(getAddressTwo());
		contactData.setlAddressMDMs(lAddressMDMs);

		final List<PhoneMDM> lPhoneMDMs = new ArrayList<PhoneMDM>();
		lPhoneMDMs.add(getPhoneMDMOne());
		// lPhoneMDMs.add(getPhoneMDMTwo());
		contactData.setlPhoneMDMs(lPhoneMDMs);
		final List<EmailMDM> lEmailMDMs = new ArrayList<EmailMDM>();

		// lEmailMDMs.add(getEmailMDMOne());
		// lEmailMDMs.add(getEmailMDMTwo());
//		final List<NationalIdentifierMDM> lNationalIdentifierMDMs = new ArrayList<NationalIdentifierMDM>();
		// lNationalIdentifierMDMs.add(getNationalIdentifierMDMOne());
		// lNationalIdentifierMDMs.add(getNationalIdentifierMDMTwo());

		final List<ContactSNMDM> lContactSNMDMs = new ArrayList<ContactSNMDM>();
		lContactSNMDMs.add(getContactSNMDMOne());
		// lContactSNMDMs.add(getContactSNMDMTwo());

//		final List<ProfessionalContractMDM> lProfessionalContractMDMs = new ArrayList<ProfessionalContractMDM>();
		// lProfessionalContractMDMs.add(getProfessionalContractMDMOne());
		// lProfessionalContractMDMs.add(getProfessionalContractMDMTwo());
		contactData.setlEmailMDMs(lEmailMDMs);
		// contactData.setlNationalIdentifierMDMDs(lNationalIdentifierMDMs);
		contactData.setlContactSNMDMs(lContactSNMDMs);
		// contactData.setlProfessionalContractMDMs(lProfessionalContractMDMs);
		MdmMapping.afficherContactMDM(contactData, LOGGER);
		//Assert.assertFalse(getContactServiceCallerOld() == null);

		RcuServiceCaller contactServiceCaller = rcuServiceCaller;
		Assert.assertNotNull(contactServiceCaller);
		try {

			c = contactServiceCaller.createContactMDM(this.APPLICATION_ID, contactData);
		} catch (final RcuException e) {
			final List<ErrorItem> errors = e.getErrors();
			if (errors != null) {
				for (final ErrorItem errorItem : errors) {
					LOGGER.info("error code :" + errorItem.getErrorCode());
					LOGGER.info("error Message :" + errorItem.getErrorInfo());
					LOGGER.info("error Field :" + errorItem.getErrorField());
					LOGGER.info("error Index :" + errorItem.getErrorIndex());
				}
			}
		}

		Assert.assertNotNull(c);
		MdmMapping.afficherContactMDM(c, LOGGER);

		LOGGER.info("--Fin du test testCreateContact--");
	}

	@SuppressWarnings("deprecation")
	public AddressMDM getAddressOne() {
		final AddressMDM addressData = new AddressMDM();

		addressData.setCompanyName("");

		addressData.setExtensionAddress("");
		addressData.setAddress1("une address quelconque");
		addressData.setAddress2("");
		
		addressData.setAddressNPAI(0);
		
		addressData.setCity("Paris");
		addressData.setCountry("FR");
		addressData.setZipCode("");
		addressData.setType("1");

		return addressData;
	}

	@SuppressWarnings("deprecation")
	public AddressMDM getAddressTwo() {
		final AddressMDM addressData = new AddressMDM();

		addressData.setCompanyName("MBR");
		addressData.setExtensionAddress("App 122");
		addressData.setAddress1("4 rue Edgar varese");
		addressData.setAddress2("Bat A Appt 144");
		addressData.setAddressNPAI(1);
		addressData.setCity("Paris");
		addressData.setCountry("FR");
		addressData.setZipCode("75019");
		addressData.setAddressBilling(1);
		addressData.setAddressNPAI(1);
		addressData.setAddressNPAIDate(this.birthday);
		addressData.setAddressOptOut(1);
		addressData.setAddressOptOutDate(this.birthday);
		addressData.setAddressOptOutDate(this.birthday);
		addressData.setPrimary(1);

		return addressData;
	}

	public PhoneMDM getPhoneMDMOne() {
		final PhoneMDM phoneData = new PhoneMDM();

		phoneData.setPhonePrefix("33");
		// phoneData.setPhoneNPAI(1);
		// phoneData.setForcageFlag(0);
		// phoneData.setPhoneNPAIDate(this.birthday);
		phoneData.setPhoneNumber("0626761692");
		// phoneData.setPhoneOptOut(1);
		// phoneData.setPhoneOptOutDate(this.birthday);

		phoneData.setPhoneType("Personal");
		// phoneData.setPrimary(1);

		phoneData.setUsageCode("Cell");
		return phoneData;
	}

	public PhoneMDM getPhoneMDMTwo() {
		final PhoneMDM phoneData = new PhoneMDM();

		phoneData.setPhoneNPAI(1);
		phoneData.setForcageFlag(0);
		phoneData.setPhoneNPAIDate(this.birthday);
		phoneData.setPhoneNumber("0622652840");
		phoneData.setPhoneOptOut(1);
		phoneData.setPhoneOptOutDate(this.birthday);
		phoneData.setPhonePrefix("44");
		phoneData.setPhoneType("Fax");
		phoneData.setPrimary(0);

		phoneData.setUsageCode("Professional");
		return phoneData;
	}

	public EmailMDM getEmailMDMOne() {
		final EmailMDM emailData = new EmailMDM();

		// emailData.setEmailNPAI(1);
		// emailData.setEmailNPAIDate(this.birthday);
		// emailData.setEmailOptOut(false);
		// emailData.setEmailOptOutDate(this.birthday);
		emailData.setEmailType("Personal");
		emailData.setLoginEmail("mojo.test@me.com");
		// emailData.setPrimary(1);
		// emailData.setRequestedEmail("testbatchmdm1_ehz@hotmail.fr");

		return emailData;

	}

	public EmailMDM getEmailMDMTwo() {
		final EmailMDM emailData = new EmailMDM();

		emailData.setEmailNPAI(1);
		emailData.setEmailNPAIDate(this.birthday);
		emailData.setEmailOptOut(true);
		emailData.setEmailOptOutDate(this.birthday);
		emailData.setEmailType("Professional");
		emailData.setLoginEmail("testbatchmdm1@gmail.fr");
		emailData.setPrimary(0);
		emailData.setRequestedEmail("zxxssddni_f@accor.fr");
		return emailData;

	}

	NationalIdentifierMDM getNationalIdentifierMDMOne() {
		final NationalIdentifierMDM niddata = new NationalIdentifierMDM();

		niddata.setExpirationDate(this.birthday);
		niddata.setIssuedDate(this.birthday);
		niddata.setIssuingCountry("FR");
		niddata.setNationalityCode("fr");
		niddata.setNumero("66543");
		niddata.setTypeCode("Active");
		niddata.setUseType("Passport");

		return niddata;

	}

	NationalIdentifierMDM getNationalIdentifierMDMTwo() {
		final NationalIdentifierMDM niddata = new NationalIdentifierMDM();

		niddata.setExpirationDate(this.ouptOutDate);
		niddata.setIssuedDate(this.ouptOutDate);
		niddata.setIssuingCountry("FR");
		niddata.setNationalityCode("fr");
		niddata.setNumero("66543333");
		niddata.setTypeCode("Active");
		niddata.setUseType("Passport");

		return niddata;

	}

	ContactSNMDM getContactSNMDMOne() {
		final ContactSNMDM snmdm = new ContactSNMDM();

		snmdm.setBrand("NOV");
		snmdm.setSnNPAIDate(this.birthday);
		snmdm.setSnNPAIFlag(1);
		snmdm.setSnOptOut(1);
		snmdm.setSnOptOutDate(this.birthday);
		snmdm.setSocialNetworkType("FACEBOOK");
		snmdm.setToken("tokenw");
		snmdm.setTokenDate(this.birthday);
		snmdm.setUID("UIDAbdelazizBar4");
		return snmdm;

	}

	ContactSNMDM getContactSNMDMTwo() {
		final ContactSNMDM snmdm = new ContactSNMDM();

		snmdm.setBrand("MER");
		snmdm.setSnNPAIDate(this.birthday);
		snmdm.setSnNPAIFlag(0);
		snmdm.setSnOptOut(0);
		snmdm.setSnOptOutDate(this.birthday);
		snmdm.setSocialNetworkType("FACEBOOK");
		snmdm.setToken("tokensse");
		snmdm.setTokenDate(this.birthday);
		snmdm.setUID("UIDChakhariwvvw");

		return snmdm;

	}

	ProfessionalContractMDM getProfessionalContractMDMOne() {
		final ProfessionalContractMDM pc = new ProfessionalContractMDM();
		pc.setCompanyID("companyID");
		pc.setCompanyName("companyName");
		pc.setContractID("247CAPCAP883");

		return pc;
	}

	ProfessionalContractMDM getProfessionalContractMDMTwo() {
		final ProfessionalContractMDM pc = new ProfessionalContractMDM();
		pc.setCompanyID("companyID2");
		pc.setCompanyName("companyName2");
		pc.setContractID("247CAPCAP883");

		return pc;
	}

}