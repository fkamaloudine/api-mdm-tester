package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.List;

public class SetLoyaltyCardsMDMTest extends AbstractRcuBaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SetLoyaltyCardsMDMTest.class);

    @Test
    public void testSetLoyaltyCardsMDM() throws Exception {
        LOGGER.info("--Debut du test testSetLoyaltyCardsMDM--");

        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        int APPLICATION_ID = 20;
        String CONTACT_PMID = "0260292A";
        String CODE_TYPE_CARTE = "A2";
        String EXP_AN_CARTE = "2019";
        String EXP_MOIS_CARTE = "12";
        String STATUS_CARTE = "1";
        String NUM_CARTE = "3081032000000603";
        String NB_NEW_POINTS = "84689489";
        String NB_OLD_POINTS = "255";
        String DATE_NEW_POINTS = "03/03/2013";
        String DATE_OLD_POINTS = "03/03/2011";

        int i = -1;
        try {
            i = contactServiceCaller.setLoyaltyCardsMDM(
                    APPLICATION_ID,
                    CODE_TYPE_CARTE,
                    CONTACT_PMID,
                    EXP_AN_CARTE,
                    EXP_MOIS_CARTE,
                    STATUS_CARTE,
                    NUM_CARTE,
                    NB_OLD_POINTS,
                    formatter.parse(DATE_OLD_POINTS),
                    NB_NEW_POINTS,
                    formatter.parse(DATE_NEW_POINTS)
            );
        } catch (RcuException e) {
            List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }

        }

        Assert.assertEquals(0, i);
        LOGGER.info("Code retour = " + i);
        LOGGER.info("--Fin du test testSetLoyaltyCardsMDM--");

    }

}
