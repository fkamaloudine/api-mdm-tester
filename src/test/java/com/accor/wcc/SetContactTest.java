/**
 * Copyright - Accor - All Rights Reserved www.accorhotels.com
 */
package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.*;
import com.accor.rcu.model.saleagreement.ProfessionalContract;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class SetContactTest extends AbstractRcuBaseTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(SetContactTest.class);

  public int applicationId = 20;
  private Long profileContactID = 135120834L;
  private String loginEmail = "RAN07frdh06@test14.com";
  private String pmid = null;
  private int primaryEmailNPAI = 0;
  private String requestedEmail = "";
  private boolean hasPassword = false;
  private boolean mustUpdatePassword = false;
  private Date lastConnectionDate = new Date();
  private String civility = "MR";
  private String firstName = "CHRISTOPHE";
  private String lastName = "DORDAIN";
  private boolean emailOptOut = false;
  private String language = "fr";
  private boolean isMerged = false;
  private boolean aClubMember = false;
  private Address addressData = new Address();
  private Phone phoneData = new Phone();
  private ProfessionalContract professionalContract = null;

  //en attente d'ajout au niveau Siebel - Orazio
  // professionalContract.setContractID("");

  private Date birthday = null;
  private Card card = null;
  private Card oldCard = null;
  private boolean reachable = true;
  private boolean hasPreference = false;
  private String noShareToHotel = null;
  //String password="zsx456";
  private String password = "password1";
  private String newPassword = null;
  private String registrationSite = null;
  private String registrationChannel = null;

  @Test
  public void testSetContact() throws Exception {
    LOGGER.info("--Debut du test testSetContact--");

    int c = -1;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    birthday = sdf.parse("25/09/1990");
    addressData.setCompanyName("AUCHAN");
    addressData.setExtensionAddress("Apt2");
    addressData.setAddress1("8 Rue Victor Hugo");
    addressData.setAddress2("Résidence");
    addressData.setCity("Paris");
    addressData.setZipCode("75017");
    addressData.setCountry("pfr");
    //addressData.setState("PCT");
    addressData.setAddressNPAI(0);
    phoneData.setPhoneNumber("0622-548736");
    phoneData.setPhonePrefix("33");
    phoneData.setPhoneNPAI(0);
    Location location = new Location(addressData, phoneData);
    Contact contactData = new Contact(profileContactID, loginEmail,
            requestedEmail, pmid, firstName, lastName, civility, password,
            newPassword, hasPassword, mustUpdatePassword, primaryEmailNPAI,
            lastConnectionDate, location, aClubMember, emailOptOut,
            isMerged, language, registrationSite, registrationChannel,
            professionalContract, birthday, card, oldCard, reachable,
            hasPreference, noShareToHotel);

    RcuServiceCaller contactServiceCaller = rcuServiceCaller;
    Assert.assertNotNull(contactServiceCaller);
    try {
      c = contactServiceCaller.setContact(10, contactData);
    } catch (RcuException e) {
      List<ErrorItem> errors = e.getErrors();
      if (errors != null) {
        for (ErrorItem errorItem : errors) {
          LOGGER.info("error code :" + errorItem.getErrorCode());
          LOGGER.info("error Message :" + errorItem.getErrorInfo());
          LOGGER.info("error Field :" + errorItem.getErrorField());
          LOGGER.info("error Index :" + errorItem.getErrorIndex());
        }
      }
    }
    Assert.assertEquals(0, c);

    LOGGER.info("Code retour : " + c);
    LOGGER.info("--Fin du test testSetContact--");
  }


}
