package com.accor.wcc;

import com.accor.rcu.client.helper.MdmMapping;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.Contact;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetContactTest extends AbstractRcuBaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetContactTest.class);

    private int applicationId = 10;
    private Long contactId = 135120834L;

    @Test
    public void testGetContact() throws Exception {
        LOGGER.info("--Test Service GetContact--");
        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);

        Contact profile = contactServiceCaller.getContact(applicationId, contactId);

        Assert.assertNotNull(profile);
        MdmMapping.afficherContact(profile, LOGGER);
        Assert.assertNotNull(profile.getContactID());
        Assert.assertEquals(contactId, profile.getContactID());
        Assert.assertEquals("55649237", profile.getPmid());
        Assert.assertEquals("test_junit_5@junittest.fn", profile.getLoginEmail());
        Assert.assertEquals(0, profile.getPrimaryEmailNPAI());
        Assert.assertNotNull(profile.getBirthday());
        Assert.assertTrue(profile.getHasPassword());
        Assert.assertFalse(profile.getMustUpdatePassword());
        Assert.assertEquals("PFR", profile.getLocation().getAddress().getCountry());

        LOGGER.info("--Fin Test Service GetContact--");
    }

}
