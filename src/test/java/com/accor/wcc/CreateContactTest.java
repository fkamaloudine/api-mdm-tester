/**
 * Copyright - Accor - All Rights Reserved www.accorhotels.com
 */
package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.helper.MdmMapping;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.*;
import com.accor.rcu.model.saleagreement.ProfessionalContract;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CreateContactTest extends AbstractRcuBaseTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(CreateContactTest.class);

  private int applicationId = 20;
  private Long profileContactID = null;
  private String loginEmail = "test_junit_4.11@junitTest.fn";
  private String pmid = null;
  private int primaryEmailNPAI = 1;
  private String requestedEmail = "";
  private boolean hasPassword = true;
  private boolean mustUpdatePassword = false;
  private Date lastConnectionDate = new Date();
  private String civility = "mr";
  private String firstName = "Test Junit";
  private String lastName = "Ne pas supprimer";
  // private  String firstName = "&é\"'(-è_çà)=?./§%µ<" ;
  // private  String lastName= "~#{[|`\\^@]}^$ù*,;:!>";
  private boolean emailOptOut = true;
  private String language = "fr";
  private boolean isMerged = false;
  private boolean aclubMember = true;
  private Address addressData = new Address();
  private Phone phoneData = new Phone();
  private ProfessionalContract professionalContract = new ProfessionalContract();
  private Date birthday = null;
  private Card card = new Card();
  private Card oldCard = null;
  private boolean reachable = true;
  private boolean hasPreference = true;
  private String noShareToHotel = null;
  private String password = "password1";
  // private String password=null;
  private String newPassword = null;
  private String registrationSite = "adg";
  private String registrationChannel = "web";

  @Test
  public void testCreateContact() throws Exception {
    LOGGER.info("--Debut du test testCreateContact--");

    Contact c = null;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    birthday = sdf.parse("25/09/1987");
    addressData.setCompanyName("AUCH");
    addressData.setExtensionAddress("Apt2");
    addressData.setAddress1("8 rue âùTEst 2Viole1");
    addressData.setAddress2("Résidence");
    addressData.setAddressNPAI(1);
    addressData.setCity("Paris");
    addressData.setCountry("br");
    // addressData.setState("bsp");
    addressData.setZipCode("75018");
    phoneData.setPhoneNumber("0624648736");
    phoneData.setPhonePrefix("33");
    phoneData.setPhoneNPAI(1);
    // professionalContract.setCompanyID("HUA11");
    // professionalContract.setContractID("HUAW11");
    // professionalContract.setCompanyName("HUAWEI11");
    card.setCardExpirationDate("01/2015");
    card.setCardNewPointNumber("200");
    card.setCardNumber("2507987");
    card.setCardOldPointNumber("100");
    card.setCardType("A2");
    Location location = new Location(addressData, phoneData);
    Contact contactData = new Contact(profileContactID, loginEmail, requestedEmail, pmid,
            firstName, lastName, civility, password, newPassword, hasPassword,
            mustUpdatePassword, primaryEmailNPAI, lastConnectionDate, location, aclubMember,
            emailOptOut, isMerged, language, registrationSite, registrationChannel,
            professionalContract, birthday, card, oldCard, reachable, hasPreference, noShareToHotel);

    RcuServiceCaller contactServiceCaller = rcuServiceCaller;
    Assert.assertNotNull(contactServiceCaller);

    try {
      c = contactServiceCaller.createContact(applicationId, contactData);
    } catch (final RcuException e) {
      final List<ErrorItem> errors = e.getErrors();
      for (final ErrorItem errorItem : errors) {
        LOGGER.info("error code :" + errorItem.getErrorCode());
        LOGGER.info("error Message :" + errorItem.getErrorInfo());
        LOGGER.info("error Field :" + errorItem.getErrorField());
        LOGGER.info("error Index :" + errorItem.getErrorIndex());
      }
    }

    Assert.assertNotNull(c);
    MdmMapping.afficherContact(c, LOGGER);

    LOGGER.info("--Fin du test testCreateContact--");
  }

}
