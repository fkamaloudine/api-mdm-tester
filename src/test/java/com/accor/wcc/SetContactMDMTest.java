package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.ContactMDM;
import com.accor.rcu.model.profile.PhoneMDM;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SetContactMDMTest extends AbstractRcuBaseTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(SetContactMDMTest.class);

    private final Long profileContactID = new Long("100001083326085");

    @Test
    public void testSetContactMDM() throws Exception {
        LOGGER.info("--Debut du test testSetContact--");
        int c = -1;
        PhoneMDM phoneData = new PhoneMDM();
        phoneData.setPhonePrefix("33");
        phoneData.setPhoneNumber("0626761692");
        phoneData.setUsageCode("Cell");
        phoneData.setPhoneType("Professional");

        ContactMDM contactData = new ContactMDM();
        String firstName = "Yannick";
        contactData.setFirstName(firstName);
        contactData.setFirstNameUpper(firstName.toUpperCase());
        String lastName = "SAGSETe";
        contactData.setLastName(lastName);
        contactData.setLastNameUpper(lastName.toUpperCase());


        contactData.setContactID(this.profileContactID);
        List<PhoneMDM> lPhoneMDMs = new ArrayList<>();
        lPhoneMDMs.add(phoneData);
        contactData.setlPhoneMDMs(lPhoneMDMs);

        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);
        try {

            int APPLICATION_ID = 20;
            c = contactServiceCaller.setContactMDM(APPLICATION_ID, contactData);

        } catch (final RcuException e) {
            final List<ErrorItem> errors = e.getErrors();
            if (errors != null) {
                for (final ErrorItem errorItem : errors) {
                    LOGGER.info("error code :" + errorItem.getErrorCode());
                    LOGGER.info("error Message :" + errorItem.getErrorInfo());
                    LOGGER.info("error Field :" + errorItem.getErrorField());
                    LOGGER.info("error Index :" + errorItem.getErrorIndex());
                }
            }

        }
        Assert.assertEquals(0, c);
        LOGGER.info("Code retour : " + c);

        LOGGER.info("--Fin du test testSetContact--");
    }

}
