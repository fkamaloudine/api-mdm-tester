package com.accor.wcc;

import com.accor.rcu.client.profile.RcuServiceCaller;
import org.junit.Before;


public abstract class AbstractRcuBaseTest {

    protected RcuServiceCaller rcuServiceCaller;

    @Before
    public final void setUp() throws Exception {
        rcuServiceCaller = ContextLoader.getInstance().getContactServiceCallerApi();
    }


}