package com.accor.wcc;

import com.accor.rcu.client.helper.MdmMapping;
import com.accor.rcu.client.profile.RcuServiceCaller;
import com.accor.rcu.model.profile.ContactMDM;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetContactMDMTest extends AbstractRcuBaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetContactMDMTest.class);

    private int applicationId = 10;
    private Long contactId = 1780177306L;//new Long(31003268); //100001082699653L //100001061287066L


    @Test
    public void testGetContactMDM() throws Exception {
        LOGGER.info("--Test Service GetContactMDM --");
        RcuServiceCaller contactServiceCaller = rcuServiceCaller;
        Assert.assertNotNull(contactServiceCaller);

        ContactMDM profile = contactServiceCaller.getContactMDM(this.applicationId, this.contactId);

        Assert.assertNotNull(profile);
        MdmMapping.afficherContactMDM(profile, LOGGER);
        Assert.assertNotNull(profile.getContactID());
        Assert.assertEquals(contactId, profile.getContactID());
        Assert.assertEquals("55649237", profile.getPmid());
        Assert.assertEquals("test_junit_5@junittest.fn", profile.getlEmailMDMs().get(0).getLoginEmail());
        Assert.assertEquals(0, profile.getPrimaryEmailNPAI());
        Assert.assertNotNull(profile.getBirthday());
        Assert.assertTrue(profile.getHasPassword());
        Assert.assertFalse(profile.getMustUpdatePassword());
        Assert.assertEquals("PBR", profile.getlAddressMDMs().get(0).getCountry());

        LOGGER.info("--Fin Test Service GetContactMDM--");
    }

}
