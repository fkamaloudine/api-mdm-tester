package com.accor.wcc;

import com.accor.rcu.client.exception.ErrorItem;
import com.accor.rcu.client.exception.RcuException;

import com.accor.rcu.client.profile.RcuServiceCaller;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ActivateRequestedEmailMDMTest extends AbstractRcuBaseTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActivateRequestedEmailMDMTest.class);

	@Test
	public void testActivateRequestedEmailTest() {
		LOGGER.info("--Debut du test ActivateRequestedEmailTest --");

		RcuServiceCaller contactServiceCaller = rcuServiceCaller;
		Assert.assertNotNull(contactServiceCaller);
		try {
			int applicationId = 35;
			String identifierKey = "f2949836eb58be49f9ca349be8e01d811b5ab9c7bf9141bb9694dbf1348146f4";
			String requestedEmail = "reqEmail@test.fr";
			int result = contactServiceCaller.activateRequestedEmail(applicationId, identifierKey, requestedEmail);
			
	        LOGGER.info("ActivateRequestedEmailTest - result: " + result);

		} catch (RcuException e) {
			List<ErrorItem> errors = e.getErrors();
			for (ErrorItem errorItem : errors) {
				LOGGER.info("error code :" + errorItem.getErrorCode());
				LOGGER.info("error Message :" + errorItem.getErrorInfo());
				LOGGER.info("error Field :" + errorItem.getErrorField());
				LOGGER.info("error Index :" + errorItem.getErrorIndex());
				Assert.assertEquals(errorItem.getErrorCode(), 0);

			}

			LOGGER.info("--Fin du test ActivateRequestedEmailTest --");
		}

	}
}
